package com.example.kjankiewicz.android_09i10w01_mythreads

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.util.Log
import android.widget.Toast

class MySecondBroadcastReceiver : BroadcastReceiver() {

    private var mPlayer: MediaPlayer? = null

    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.action
        Log.d("MySecondBroadcastRec", "action: " + action!!)
        if (action == ACTION_PLAY_MUSIC) {
            val results = getResultExtras(true)
            var fileNo = results.getInt(FILE_NO, 0)
            if (fileNo == 0)
                fileNo = intent.getIntExtra(FILE_NO, 0)
            Log.d("MySecondBroadcastRec", "fileNo: $fileNo")
            if (mPlayer != null && mPlayer!!.isPlaying)
                mPlayer!!.stop()

            if (fileNo == 1) {
                mPlayer = MediaPlayer.create(
                        context,
                        R.raw.iron_butterfly_sample)
                mPlayer!!.start()
                try {
                    Thread.sleep(5000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
                results.putInt(FILE_NO, 2)
            } else {
                mPlayer = MediaPlayer.create(
                        context,
                        R.raw.jody_grind_sample)
                mPlayer!!.start()

            }
        }
        Toast.makeText(context, "(2) Is not save... from now",
                Toast.LENGTH_SHORT).show()
    }

    companion object {
        const val ACTION_PLAY_MUSIC = "com.example.kjankiewicz.android_09i10w01_mythreads.ACTION_PLAY_MUSIC"
        const val FILE_NO = "com.example.kjankiewicz.android_09i10w01_mythreads.FILE_NO"
    }
}
