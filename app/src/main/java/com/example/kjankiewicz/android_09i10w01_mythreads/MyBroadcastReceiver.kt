package com.example.kjankiewicz.android_09i10w01_mythreads

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.telephony.TelephonyManager
import android.util.Log
import android.widget.Toast

class MyBroadcastReceiver : BroadcastReceiver() {

    private var mPlayer: MediaPlayer? = null

    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.action
        Log.d("MyBroadcastReceiver", "action: $action")
        when (action) {
            ACTION_PLAY_MUSIC -> {
                val results = getResultExtras(true)
                var fileNo = results.getInt(FILE_NO, 0)
                if (fileNo == 0)
                    fileNo = intent.getIntExtra(FILE_NO, 0)
                Log.d("MyBroadcastReceiver", "fileNo: $fileNo")
                if (mPlayer != null && mPlayer!!.isPlaying)
                    mPlayer!!.stop()

                if (fileNo == 1) {
                    mPlayer = MediaPlayer.create(
                            context,
                            R.raw.iron_butterfly_sample)
                    mPlayer!!.start()
                } else {
                    mPlayer = MediaPlayer.create(
                            context,
                            R.raw.jody_grind_sample)
                    mPlayer!!.start()
                }
            }
            "android.intent.action.BOOT_COMPLETED" -> {
                if (mPlayer != null && mPlayer!!.isPlaying)
                    mPlayer!!.stop()
                mPlayer = MediaPlayer.create(
                        context,
                        R.raw.iron_butterfly_sample)
                mPlayer!!.start()
            }
            "android.intent.action.PHONE_STATE" -> {
                val extras = intent.extras
                Log.d("MyBroadcastReceiver", "bundle: " + (extras != null))
                if (extras != null) {
                    val state = extras.getString(TelephonyManager.EXTRA_STATE)
                    Log.d("MyBroadcastReceiver", "state: $state")
                    if (state == TelephonyManager.EXTRA_STATE_RINGING) {
                        val phoneNumber = extras
                                .getString(TelephonyManager.EXTRA_INCOMING_NUMBER)
                        Log.d("MyBroadcastReceiver", "phoneNumber: $phoneNumber")
                        if (phoneNumber!= null && phoneNumber.startsWith("2")) {
                            if (mPlayer != null && mPlayer!!.isPlaying)
                                mPlayer!!.stop()
                            mPlayer = MediaPlayer.create(
                                    context,
                                    R.raw.jody_grind_sample)
                            mPlayer!!.start()
                        }
                    }
                }
            }
        }
        Toast.makeText(context, "(1) Is not save... from now",
                Toast.LENGTH_SHORT).show()
    }

    companion object {

        const val ACTION_PLAY_MUSIC = "com.example.kjankiewicz.android_09i10w01_mythreads.ACTION_PLAY_MUSIC"
        const val FILE_NO = "com.example.kjankiewicz.android_09i10w01_mythreads.FILE_NO"
    }
}
