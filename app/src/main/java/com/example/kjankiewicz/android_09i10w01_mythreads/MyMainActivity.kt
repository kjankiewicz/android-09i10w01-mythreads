package com.example.kjankiewicz.android_09i10w01_mythreads

import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ImageView
import android.widget.Toast

import java.io.IOException
import java.lang.ref.WeakReference
import java.net.HttpURLConnection
import java.net.URL
import java.util.Calendar

import kotlinx.android.synthetic.main.activity_my_main.*

class MyMainActivity : AppCompatActivity() {
    internal lateinit var img01ImageView: ImageView
    private lateinit var mBroadcastReceiver: MyBroadcastReceiver
    private lateinit var mSecondBroadcastReceiver: MySecondBroadcastReceiver

    /* Bad solutions - leakage of memory due to implicit reference to Activity

    private inner class DownloadImageTask : AsyncTask<String, Void, Bitmap>() {

        override fun doInBackground(vararg urls: String): Bitmap? {
            return loadImageFromNetwork(urls[0])
        }

        override fun onPostExecute(result: Bitmap) {
            img01ImageView.setImageBitmap(result)
        }
    }

    private inner class MyMainActivityHandler : Handler() {

        override fun handleMessage(msg: Message) {
            img01ImageView.setImageBitmap(msg.obj as Bitmap)
        }
    }*/



    private class DownloadImageTask// only retain a weak reference to the activity
    internal constructor(context: MyMainActivity) : AsyncTask<String, Void, Bitmap>() {

        private val activityReference: WeakReference<MyMainActivity> = WeakReference(context)

        override fun doInBackground(vararg urls: String): Bitmap? {
            val activity = activityReference.get()
            return if (activity == null || activity.isFinishing) null
            else activity.loadImageFromNetwork(urls[0])
        }

        override fun onPostExecute(result: Bitmap) {
            val activity = activityReference.get()
            if (activity == null || activity.isFinishing) return

            activity.img01ImageView.setImageBitmap(result)
        }
    }

    private class MyMainActivityHandler// only retain a weak reference to the activity
    internal constructor(context: MyMainActivity) : Handler() {

        private val activityReference: WeakReference<MyMainActivity> = WeakReference(context)

        override fun handleMessage(msg: Message) {
            val activity = activityReference.get()
            if (activity == null || activity.isFinishing) return

            activity.img01ImageView.setImageBitmap(msg.obj as Bitmap)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_main)

        mBroadcastReceiver = MyBroadcastReceiver()
        val intentFilter = IntentFilter(MyBroadcastReceiver.ACTION_PLAY_MUSIC)
        intentFilter.priority = 1
        registerReceiver(mBroadcastReceiver, intentFilter)

        mSecondBroadcastReceiver = MySecondBroadcastReceiver()
        intentFilter.priority = 2
        registerReceiver(mSecondBroadcastReceiver, intentFilter)

        img01ImageView = findViewById(R.id.img01_imageView)

        longBadButton.setOnClickListener {
            synchronized(this) {
                try {
                    Thread.sleep(10000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

            }
        }

        downloadImg01Button.setOnClickListener {
            val bm = BitmapFactory.decodeResource(
                    resources,
                    R.raw.pusty_obraz)
            img01ImageView.setImageBitmap(bm)
            when {
                uiThreadRadioButton.isChecked -> {
                    val b = loadImageFromNetwork(imgFile)
                    img01ImageView.setImageBitmap(b)
                }
                secondViolatedRadioButton.isChecked ->
                    Thread {
                        val b = loadImageFromNetwork(imgFile)
                        img01ImageView.setImageBitmap(b)
                    }.start()
                runOnUiThreadRadioButton.isChecked ->
                    Thread {
                        val bitmap = loadImageFromNetwork(imgFile)
                        runOnUiThread { img01ImageView.setImageBitmap(bitmap) }
                    }.start()
                viewPostRadioButton.isChecked ->
                    Thread {
                        val bitmap = loadImageFromNetwork(imgFile)
                        img01ImageView.post { img01ImageView.setImageBitmap(bitmap) }
                    }.start()
                viewPostDelayedRadioButton.isChecked ->
                    Thread {
                        val bitmap = loadImageFromNetwork(imgFile)
                        val startTime = Calendar.getInstance().timeInMillis
                        img01ImageView.postDelayed({
                            Toast.makeText(applicationContext, "delayed: " +
                                    (Calendar.getInstance().timeInMillis - startTime).toString(),
                                    Toast.LENGTH_SHORT).show()
                            img01ImageView.setImageBitmap(bitmap)
                        }, 3000)
                    }.start()

                handlerRadioButton.isChecked -> {
                    val myHandler: Handler
                    myHandler = MyMainActivityHandler(this)
                    Thread {
                        val msg = myHandler.obtainMessage()
                        msg.obj = loadImageFromNetwork(imgFile)
                        myHandler.sendMessage(msg)
                    }.start()
                }
                asyncTaskRadioButton.isChecked ->
                    DownloadImageTask(this).execute(imgFile)
            }
        }

        niceButton.setOnClickListener {
            val intent = Intent()
            intent.action = MyBroadcastReceiver.ACTION_PLAY_MUSIC
            intent.putExtra(MyBroadcastReceiver.FILE_NO, 1)
            sendOrderedBroadcast(intent, null)
        }

        justButton.setOnClickListener {
            val intent = Intent()
            intent.action = MyBroadcastReceiver.ACTION_PLAY_MUSIC
            intent.putExtra(MyBroadcastReceiver.FILE_NO, 2)
            //LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
            sendBroadcast(intent, null)
        }

    }

    fun loadImageFromNetwork(urlFile: String): Bitmap? {
        try {
            val url = URL(urlFile)
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            Log.d("loadImageFromNetwork", "response: " + connection.responseCode.toString())
            val input = connection.inputStream
            return BitmapFactory.decodeStream(input)

        } catch (e: IOException) {
            e.printStackTrace()
        }

        return null
    }

    /*@Override
    public void onResume() {
        super.onResume();
        myReceiver = new MyBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter(MyBroadcastReceiver.ACTION_PLAY_MUSIC);
        registerReceiver(myReceiver, intentFilter);
        //LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,intentFilter);
    }*/

    public override fun onPause() {
        super.onPause()
        unregisterReceiver(mBroadcastReceiver)
        unregisterReceiver(mSecondBroadcastReceiver)
    }

    companion object {

        internal var imgFile = "http://jankiewicz.pl/images/yellow_oak1200x400.jpg"
    }
}
